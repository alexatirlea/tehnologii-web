import React, { Component } from 'react';
import './App.css';
import {EventEmitter} from 'fbemitter'
import CategoriesSelect from './components/CategoriesSelect'
import ArtistSelect from './components/ArtistSelect'
import ArtistDisplay from './components/ArtistDisplay'
import CategoriesStore from './stores/CategoriesStore'
import ArtistStore from './stores/ArtistStore'
import ArtistAddForm from './components/ArtistAddForm'
import CreationStore from './stores/CreationStore'

const ee = new EventEmitter()
const storeCategories = new CategoriesStore(ee)
const storeArtist = new ArtistStore(ee)
const storeCreation = new CreationStore(ee)


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory : {},
      selectedArtist : {},
      categories : [],
      artists : [],
      creations : [],
      editMode: false,
      adding: false,
      called: false
    }
    this.getCreations = this.getCreations.bind(this);
  }
  
  getCreations(artist) {
    var selectedArtist = artist;
    if(this.state !== undefined) {
      selectedArtist = this.state.selectedArtist;
    }
      storeCreation.getCreations(selectedArtist.id);
      ee.addListener('CREATIONS_LOAD', () => {
      this.setState({
        creations : storeCreation.content,
      })
    })
  }

  componentDidMount() {
    storeCategories.getCategories();
    ee.addListener('CATEGORIES_LOAD', () => {
      this.setState({
        categories : storeCategories.content,
        selectedCategory: storeCategories.content[0]
      })
    })

    storeArtist.getArtists();
    ee.addListener('ARTISTS_LOAD', () => {
      
      this.setState({
        artists : storeArtist.content,
      })
      var category = this.findCategory(this.state.selectedCategory.category_name)
      var artist = this.findArtistFromCategory(category[0]);
      this.setState({
        selectedArtist: artist
      })
    this.getCreations(artist);
    })
    
  }

  selectOption = (label, option) => {
    if(label === 'Categories') {
      var category = this.findCategory(option)[0]
      this.setState(prevState => ({
        selectedCategory: category,
        selectedArtist : this.findArtistFromCategory(category),
        editMode: false,
        adding: false
      }), () => this.getCreations())
    } else if(label === 'Artists') {
      this.setState(prevState => ({
        selectedArtist: this.findArtist(option)[0],
        editMode: false,
        adding: false
      }), () => this.getCreations())
    }
  }

  findArtistFromCategory = (category) => {
    if(category !== undefined) {
      var result = this.state.artists.filter((e) => category.id === e.category_id);
      if(result.length > 0) {
        return result[0];
      } else {
        return {};
      }
    }
    return {};
  }

  findId = (category) => {
    var result = this.state.categories.filter((e) => e.category_name === category.category_name).map((e) => e.id)[0];
    if(this.state.called === false) {
      this.getCreations();
      this.setState(prevState => ({
        called: true
      }))
    }
    return result;
  }

  findIdFromLabel = (label) => {
    var result = this.state.categories.filter((e) => e.category_name === label).map((e) => e.id)[0];
    return result;
  }

  findArtist(label) {
    var artist = this.state.artists.filter((e) => e.firstname + ' ' + e.lastname === label);
    return artist;
  }

  findCategory(label) {
    var category = this.state.categories.filter((e) => e.category_name === label);
    return category;
  }

  onAddArtist = (artist) => {
    artist.category_id = this.findIdFromLabel(artist.category_id);
    storeArtist.createOne(artist);
  }

  onAddCreation = (creation) => {
    creation.author_id = this.state.selectedArtist.id;
    storeCreation.createOne(creation,this.state.selectedArtist);
  }

  deleteArtist = () => {
    storeArtist.deleteOne(this.state.selectedArtist);
  }

  setEditModeTrue = () => {
    this.setState({
        editMode : true
    })
  }

  setAddingTrue = () => {
    this.setState({
        adding : true
    })
    
  }

  setAddingFalse = () => {
    this.setState({
        adding : false
    })
  }

  setEditFalse = () => {
    this.setState({
        editMode : false
    })
  }

  onEditArtist = (artist) => {
    artist.id = this.state.selectedArtist.id;
    artist.category_id = this.findIdFromLabel(artist.category_id);
    storeArtist.modifyOne(artist);
    this.setEditFalse();
  }

  render() {
    const { selectedCategory,selectedArtist, categories, artists, creations, editMode, adding} = this.state;
    var cancelButtonEdit = <button className="btn btn-sm btn-warning" onClick={this.setEditFalse}>Cancel</button>;
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-5">
            <CategoriesSelect 
              selectedCategory={selectedCategory} 
              categories={categories} 
              selectOption={this.selectOption} />

            {selectedCategory !== {} ?
               <ArtistSelect 
                  artists = { artists } 
                  findId={this.findId} 
                  selectedArtist={selectedArtist} 
                  categoryId={this.findId(selectedCategory)} 
                  selectOption={this.selectOption} /> 
               : null
            }
            <br />

            {!(Object.keys(selectedArtist).length === 0 
              && this.state.selectedArtist.constructor === Object) 
              && !this.state.editMode ?
               <ArtistDisplay 
                  onAdd={this.onAddCreation} 
                  creations={creations}
                  setEditModeTrue={this.setEditModeTrue} 
                  deleteArtist={this.deleteArtist} 
                  selectedArtist={selectedArtist} 
                  getCreations={this.getCreations}/> 
                  : null
            }

            {this.state.editMode === true ?
              <ArtistAddForm 
                  buttonLabel="Modify" 
                  editMode={editMode} 
                  categories={categories} 
                  onEditArtist={this.onEditArtist} 
                  selectedArtist={selectedArtist} />
                  : null
            }
            {editMode ?
                cancelButtonEdit:
                null
            } 
            <br />
            
          </div>
          <div className="col-sm-offset-2 col-sm-5">

            {this.state.adding ?
                <ArtistAddForm 
                  buttonLabel="Add" 
                  adding={adding} 
                  editMode={editMode} 
                  categories={categories} 
                  onAdd={this.onAddArtist} 
                  setAddingFalse={this.setAddingFalse}/> :
                <button 
                  className="btn btn-sm btn-success" 
                  onClick={this.setAddingTrue}>Add new Artist</button>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
