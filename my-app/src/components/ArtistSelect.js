import React, { Component } from 'react'

class ArtistSelect extends Component {
  state = {}

  constructor(props) {
    super(props);
    this.state = {value: this.props.selectedArtist};

    this.handleChange = this.handleChange.bind(this); 
  }

  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.selectOption("Artists", event.target.value);
  }

  render() {
    var options = this.props.artists.filter((e) => this.props.categoryId === e.category_id).map((e)=> e.firstname + ' ' +  e.lastname)
      return (
      <div>
        <label>Artists</label>
        <br />
        <select  value={this.state.value} className="form-control" onChange={this.handleChange}>
          {options.map((option,index) => 
            <option key={index}>{option}</option>
          )}
        </select>
      </div>
    ) 
  }
}

export default ArtistSelect
