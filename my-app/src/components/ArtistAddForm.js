import React, { Component } from 'react'

class ArtistAddForm extends Component {
  constructor(props) {
    super(props)
    if(!(this.props.buttonLabel === "Add")) {
      var artist = this.props.selectedArtist;
      this.state = {
        firstname : artist.firstname,
        lastname : artist.lastname,
        comments : artist.comments,
        watchers : artist.watchers,
        favorites : artist.favorites,
        deviations : artist.deviations,
        page_views : artist.page_views,
        category_id : this.findCategoryFromId(artist.category_id)
      }
    } else {
      this.state = {
        firstname : '',
        lastname : '',
        comments : '',
        watchers : '',
        favorites : '',
        deviations : '',
        page_views : '',
        category_id : ''
      }
    }
    
    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  findCategoryFromId(id) {
    var category_name = this.props.categories.filter((e) => e.id === id)[0].category_name;
    return category_name;
  }

  handleSubmit () {
    if(this.props.buttonLabel === "Add") {
      this.props.onAdd({
        category_id :this.state.category_id,
        firstname : this.state.firstname, 
        lastname : this.state.lastname,
        comments : this.state.comments,
        watchers : this.state.watchers,
        favorites : this.state.favorites,
        deviations : this.state.deviations,
        page_views : this.state.page_views
      })
      this.myFormRef.reset();
      this.props.setAddingFalse();
      alert("Artist added!");
    } else {
      this.props.onEditArtist({
        category_id : this.state.category_id,
        firstname : this.state.firstname, 
        lastname : this.state.lastname,
        comments : parseInt(this.state.comments,10),
        watchers : parseInt(this.state.watchers,10),
        favorites : parseInt(this.state.favorites,10),
        deviations : parseInt(this.state.deviations,10),
        page_views : parseInt(this.state.page_views,10)
      });
      alert("Artist modified!");
    }
    
  }

  initializeCategory () {
      if(this.state.category_id.length === 0 && this.props.categories.length > 0) {
        this.setState({
          category_id : this.props.categories[0].category_name
        })
      }
  }

  render() {
    var options = this.props.categories.map((e)=> e.category_name);
    var cancelButtonAdd = <button className="btn btn-sm btn-warning" onClick={this.props.setAddingFalse}>Cancel</button>;

    this.initializeCategory();
    return (
      <div className="form">
      <h4>Add a new Artist</h4>
      <form ref={(el) => this.myFormRef = el}>
        Firstname : <input value={this.state.firstname} className="form-control" type="text" name="firstname" onChange={this.handleChange} required/>
        Lastname : <input value={this.state.lastname} className="form-control" type="text" name="lastname" onChange={this.handleChange} required/>
        Comments : <input value={this.state.comments} className="form-control" type="number" name="comments" onChange={this.handleChange} required/>
        Watchers : <input value={this.state.watchers} className="form-control" type="number" name="watchers" onChange={this.handleChange} required/>
        Favorites : <input value={this.state.favorites} className="form-control" type="number" name="favorites" onChange={this.handleChange} required/>
        Deviations : <input value={this.state.deviations} className="form-control" type="number" name="deviations" onChange={this.handleChange} required/>
        Page views : <input value={this.state.page_views} className="form-control" type="number" name="page_views" onChange={this.handleChange} required/>
        Category :
        <select  value={this.state.category_id} name="category_id" className="form-control" onChange={this.handleChange}>
          {options.map((option,i)=> 
            <option key={i}>{option}</option>
          )}
        </select>

        <br />
        <input
          id="inputButton" 
          className="btn btn-sm btn-success" 
          type="button" 
          value={this.props.buttonLabel} 
          onClick={this.handleSubmit} ></input>

          {this.props.adding ?
                cancelButtonAdd:
                null
          }
          </form>
      </div>
    )
  }
}

export default ArtistAddForm
