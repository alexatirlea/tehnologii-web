import React, { Component } from 'react'

class CategoriesSelect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.selectedCategory
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
    this.props.selectOption("Categories", event.target.value);
  }

  render() {
    var options = this.props.categories.map((e)=> e.category_name);
    return (
      <div>
        <label>Categories</label>
        <br />
        <select  value={this.state.value} className="form-control" onChange={this.handleChange}>
          {options.map((option,i)=> 
            <option key={i}>{option}</option>
          )}
        </select>
      </div>
    )
  }
}

export default CategoriesSelect
