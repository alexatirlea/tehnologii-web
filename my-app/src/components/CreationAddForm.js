import React, { Component } from 'react'

class CreationAddForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      title : ''
    }
    this.handleChange = (event) => {
      this.setState({
         [event.target.name] : event.target.value //title : event.target.value 
      })

    }
  }

  handleSubmit = () => {
    this.props.onAdd({
        title :this.state.title
    })
    this.myFormRef.reset();  //sa se stearga ce am introdus in control-ul input
    alert("Creation added!");
  }

render() {
    return (
      <div className="form">
      <h4>Add a new Creation</h4>
      <form ref={(el) => this.myFormRef = el}> 
        Title : <input className="form-control" type="text" name="title" onChange={this.handleChange} required/>  
        <br />
        <input                  //pt buton
          id="inputButton" 
          className="btn btn-sm btn-success" 
          type="button" 
          value="add" 
          onClick={this.handleSubmit}/>
          </form>
      </div>
    )
  }
}

export default CreationAddForm
