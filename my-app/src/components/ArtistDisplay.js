import React, { Component } from 'react'
import CreationAddForm from './CreationAddForm'

var artist;

class ArtistDisplay extends Component {

  componentDidMount() {
   this.props.getCreations(this.props.selectedArtist);
  }

  render() {
    artist = this.props.selectedArtist;
    return (
      <div>
      <div className="artistCard">
        <div>
          <div>
            <h4>Name: {artist.firstname} {artist.lastname}</h4>
            <button className="info btn-sm btn-success" disabled>Watchers: {artist.watchers}</button>
            <button className="info btn-sm btn-success" disabled>Page views: {artist.page_views}</button>
            <button className="info btn-sm btn-success" disabled>Comments: {artist.comments}</button>
            <button className="info btn-sm btn-success" disabled>Favorites: {artist.favorites}</button>
            <button className="info btn-sm btn-success" disabled>Deviations: {artist.deviations}</button>
          </div>
        </div>
        <br />
        <button className="btn btn-danger btn-sm" onClick={this.props.deleteArtist}>Delete</button>
        <button className="btn btn-primary btn-sm" onClick={this.props.setEditModeTrue}>Edit</button>
      </div>
      <br />
      {this.props.creations.length > 0 ?
        <div>
           <table className="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Title</th>
                </tr>
              </thead>
              <tbody>
              {this.props.creations.map((creation,i)=> (
                <tr key={i*3+1}>
                  <th key={i} scope="row">{creation.id}</th>
                  <td key={i*2+1}>{creation.title}</td>
                </tr>
              ))}
              </tbody>
            </table>
             </div> : null
      }
        <br />
        <CreationAddForm onAdd={this.props.onAdd}/>
      </div>
    )
  }
}

export default ArtistDisplay
