import axios from 'axios'

const SERVER = 'http://localhost:8000'

class ArtistStore {
  constructor(ee) {
    this.content = []
    this.ee = ee
  }

  getArtists() {
    axios(SERVER + '/artists')
      .then((response) => {
        this.content = response.data
        this.ee.emit('ARTISTS_LOAD')
        console.log('Response from ArtistStore', response.data);
      })
      .catch((error) => console.warn(error))
  }

  createOne(artist) {
  axios.post(SERVER + '/artist', artist)
      .then(() => this.getArtists())
      .catch((error) => console.warn(error))
  }

  deleteOne(artist) {
  axios.delete(SERVER + '/artist/' + artist.id)
      .then(() => this.getArtists())
      .catch((error) => console.warn(error))
  }

  modifyOne(artist) {
  axios.put(SERVER + '/artist/' + artist.id, artist)
      .then(() => this.getArtists())
      .catch((error) => console.warn(error))
  }
}

export default ArtistStore
