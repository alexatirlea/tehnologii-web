import axios from 'axios'

const SERVER = 'http://localhost:8000'

class CreationStore {
  constructor(ee){
    this.content = []
    this.ee = ee
  }
  getCreations(artistId) {
    axios(SERVER + '/creation/' + artistId)
      .then((response) => {
        this.content = response.data
        this.ee.emit('CREATIONS_LOAD')
        console.log('Response from CreationStore', response.data);
      })
      .catch((error) => console.warn(error))
  }

  createOne(creation,artist) {
  axios.post(SERVER + '/creation', creation)
      .then(() => this.getCreations(artist.id))
      .catch((error) => console.warn(error))
  }

}

export default CreationStore
