import axios from 'axios'

const SERVER = 'http://localhost:8000'

class CategoriesStore {
  constructor(ee) {
    this.content = []
    this.ee = ee
  }
  getCategories() {
    axios(SERVER + '/categories')
      .then((response) => {
        this.content = response.data
        this.ee.emit('CATEGORIES_LOAD')
        console.log('Response from CategoriesStore', response.data);
      })
      .catch((error) => console.warn(error))
  }
}

export default CategoriesStore