Obligatoriu:

	nodejs (https://nodejs.org/en/)
	mysql server
	pt a downloada dependintele se va rula npm install in command prompt
	pt a deschide server-ul se va rula " node server.js" in command prompt
	realizez o noua bd cu denumirea artists
	se scrie adresa localhost:8080/create intr-un browser pt a realiza tabelele din bd

Endpoint-uri:

	GET /artists - vector de artisti
	GET /categories - vector de categorii
	GET /user/:id - un utilizator cu id-ul din link
	GET/creation/:id - toate creatiile utilizatorului cu id-ul din link
	POST /artist - artist nou
	POST /creation - creatie noua
	POST /user - utilizator noua
	DELETE /artist/:id - sterge artist cu id-ul din link
	PUT /artist/:id - modifica artistul cu id-ul din link