'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const sequelize = new Sequelize('artists', 'root', 'alexandra96', {
  host: 'localhost',
});

let app = express()
app.use(bodyParser.json())
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


let Artist = sequelize.define('artist', {
  firstname: {
    allowNull: false,
    type: Sequelize.STRING
  },
  lastname: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  comments: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  watchers: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  favorites: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  deviations: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  page_views: {
    allowNull: false,
    type: Sequelize.INTEGER,
  }
})

let Category = sequelize.define('category', {
  category_name: {
    allowNull: false,
    type: Sequelize.STRING
  }
})

let Creation = sequelize.define('creation', {
  title: {
    allowNull: false,
    type: Sequelize.STRING
  }
})

let User = sequelize.define('user', {
  lastname: {
    allowNull: false,
    type: Sequelize.STRING
  },
  firstname: {
    allowNull: false,
    type: Sequelize.STRING
  },
  email: {
    allowNull: false,
    type: Sequelize.STRING
  },
  phone: {
    allowNull: false,
    type: Sequelize.STRING
  },
  username: {
    allowNull: true,
    type: Sequelize.STRING
  },
  password: {
    allowNull: true,
    type: Sequelize.STRING
  }

})



Category.hasMany(Artist, {
  foreignKey: 'category_id'
})

Artist.belongsTo(Category, {
  foreignKey: 'category_id'
})

Artist.hasMany(Creation, {
  foreignKey: 'author_id'
})

Creation.belongsTo(Artist, {
  foreignKey: 'author_id',
})



app.get('/create', (req, res) => {
  sequelize
    .sync({
      force: true
    })
    .then(() => {
      res.status(201).send('created')
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})



app.get('/artists', (req, res) => {
  Artist
    .findAll({
      attributes: ['id', 'firstname','lastname','comments','watchers','favorites','deviations','page_views','category_id']
    })
    .then((artists) => {
      res.status(200).send(artists)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})

app.get('/categories', (req, res) => {
  Category
    .findAll({
      attributes: ['id', 'category_name']
    })
    .then((categories) => {
      res.status(200).send(categories)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})

app.get('/creation/:id', (req, res) => {
  Creation
    .findAll({
      attributes: ['id', 'title','author_id'],
      where: {
        author_id: req.params.id
      }
    })
    .then((creations) => {
      res.status(200).send(creations)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})

app.get('/user/:id', (req, res) => {
  User
    .find({
      attributes: ['id', 'email','username','password', 'phone','firstname','lastname'],
      where: {
        id: req.params.id
      }
    })
    .then((users) => {
      res.status(200).send(users)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})



app.post('/user', (req, res) => {
  User
    .create(req.body)
    .then(() => {
      res.status(201).send('created')
    })
    .catch((error) => {
      console.warn(error)
      console.log(req.body)
      res.status(500).send('error')

    })
})

app.post('/artist', (req, res) => {
  Artist
    .create(req.body)
    .then(() => {
      res.status(201).send('created')
    })
    .catch((error) => {
      console.warn(error)
      console.log(req.body)
      res.status(500).send('error')
    })
})

app.post('/creation', (req, res) => {
  Creation
    .create(req.body)
    .then(() => {
      res.status(201).send('created')
    })
    .catch((error) => {
      console.warn(error)
      console.log(req.body)
      res.status(500).send('error')
    })
})



app.delete('/artist/:id', (req, res) => {
  Artist
    .find({
      where: {
        id: req.params.id
      }
    })
    .then((artist) => {
      return artist.destroy()
    })
    .then(() => {
      res.status(201).send('removed')
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})



app.put('/artist/:id', (req, res) => {
  Artist
    .find({ 
      where: {
        id: req.params.id
      }
    })
    .then((artist) => {
      return artist.updateAttributes(req.body)
    })
    .then(() => {
      res.status(201).send('modified')
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('error')
    })
})


app.listen(8000)
